import sys
import numpy as np
import scipy.spatial.distance as dist
import numpy.linalg as linalg
import matplotlib.pyplot as plt
import matplotlib
from scipy.stats import multivariate_normal
from scipy.stats.mstats import gmean
from scipy.integrate import dblquad
import scipy
import copy
from scipy import stats
import rasterio as rio
import numpy as np
import utm
from scipy import signal
import time
import pickle
import json

matplotlib.rc('xtick', labelsize=16) 
matplotlib.rc('ytick', labelsize=16)


class Data():
    def __init__(self, data_file, BLCoord, TRCoord, labels, d0=1, noiseratio=[0.2,0.2,0.2,0.2]):
        '''
        Input
        ----
            data_file: file in the json format 
            labels:    list, four types of receivers
            BLCoord:   tuple, 2D coordinates as the lower left boundary of the area of interest
            TRCoord:   tuple, 2D coordinates as the upper right boundary of the area of interest
            d0:        reference distance in meters
            noiseratio: list, ratio below which is noise

            rss: dictionary, RSS vector for each receiver type
            dis: dictionary, distance vector for each receiver type
            txcoords: dictionary, 2D TX coordinates for each receiver type
            rxcoords: dictionary, 2D RX coordinates for each receiver type
        '''
        with open(data_file) as f:
            self.meta_data = json.load(f)
        self.BLCoord = BLCoord
        self.TRCoord = TRCoord
        self.labels = labels
        self.d0 = d0
        self.noiseratio = noiseratio

        self.rss = {}
        self.dis = {}
        self.txcoords = {}
        self.rxcoords = {}

    def calcData_wLabels(self, debug=1):
        '''
        Function: load received power samples, distance, rx and tx coordinates
        Output
        ----
            rssLinks: dictionary, RSS vector for each receiver type
            disLinks: dictionary, distance vector for each receiver type
            rxCoords: dictionary, 2D RX coordinates for each receiver type
            txCoords: dictionary, 2D TX coordinates for each receiver type
        '''
        rssLinks, disLinks, rxCoords, txCoords = {}, {}, {}, {}
        for label in self.labels:
            rssLinks[label] = []
            disLinks[label] = []
            rxCoords[label] = []
            txCoords[label] = []
        
        for _, key in enumerate(self.meta_data):
            rxtxmeta = self.meta_data[key]
            subkeys = list(rxtxmeta.keys())
            # One TX only
            if 'tx_coords' in subkeys and len(rxtxmeta['tx_coords'])==1:
                rx_data = rxtxmeta['rx_data']
                
                for j in range(len(rx_data)):
                    # check if coordinates are within the area range
                    # [utm_east, utm_north, utm_zone, utm_letter]
                    utmrx = utm.from_latlon(rx_data[j][1],rx_data[j][2])[:2]
                    utmtx = utm.from_latlon(rxtxmeta['tx_coords'][0][0], rxtxmeta['tx_coords'][0][1])[:2]
                    
                    if self.BLCoord[0]<=utmrx[0]<=self.TRCoord[0] and self.BLCoord[0]<=utmtx[0]<=self.TRCoord[0] \
                    and self.BLCoord[1]<=utmrx[1]<=self.TRCoord[1] and self.BLCoord[1]<=utmtx[1]<=self.TRCoord[1] \
                    and rx_data[j][0]>-150:
                        # calulate distance
                        distance = np.sqrt(sum( (np.array(utmrx)-np.array(utmtx))**2 ))

                        for label in self.labels:
                            # Excluding certain receivers for outlier removal
                            # excluded: bes-base station, 4604-bus, law73 and ebc-fixed endpoints
                            if label in rx_data[j][3] and '-bes' not in rx_data[j][3] \
                            and '4604' not in rx_data[j][3] and 'law73-nuc1'  not in rx_data[j][3]\
                            and 'ebc-nuc1'  not in rx_data[j][3]: 
                                # save rss, distance, tx and rx coordinates
                                rssLinks[label].append(rx_data[j][0])
                                disLinks[label].append(distance)

                                rxCoords[label].append(utmrx) 
                                txCoords[label].append(utmtx) 

        for i, label in enumerate(self.labels):
            if rssLinks[label]:
                rssLinks[label] = np.array(rssLinks[label])
                disLinks[label] = np.array(disLinks[label])
                rxCoords[label] = np.vstack(rxCoords[label])
                txCoords[label] = np.vstack(txCoords[label])
                if debug:
                    print('[Debug-calcRSSAllOutdoor] rss dis shape',label, np.shape(rssLinks[label]), np.shape(disLinks[label]))
                    print('[Debug-calcRSSAllOutdoor] var',label, rssLinks[label].var(),'\n')

                # select RSS measurements higher than the noise floor
                RssThresh = np.quantile(rssLinks[label], self.noiseratio[i])
                idx = rssLinks[label]>RssThresh

                self.rss[label] = rssLinks[label][idx]
                self.dis[label] = disLinks[label][idx]
                self.txcoords[label] = txCoords[label][idx,:]
                self.rxcoords[label] = rxCoords[label][idx,:]
                if debug:
                    print('\n\n')
            else:
                print('[Warning] {} is not included'.format(label))
            
# Dataset split
def SepTrainTestLink(dataArr, disArr, TrainInd, DevInd, TestInd):
    '''
    Input
    ----
        dataArr: RSS array
        disArr: link distance array
        TrainInd, DevInd, TestInd: Indices for training, validation, and testing
    Output
    ----
        dataTrain, disTrain: RSS and link distance array for training
        dataDev, disDev: similar format for validation (development)
        dataTest, disTest: similar format for testing
    '''
    dataTrain = dataArr[TrainInd]
    disTrain = disArr[TrainInd]
    
    dataDev = dataArr[DevInd]
    disDev = disArr[DevInd]
    
    dataTest = dataArr[TestInd]
    disTest = disArr[TestInd]
    return dataTrain, disTrain, dataDev, disDev, dataTest, disTest

# Calculate total fading loss
def calcPathLossRecLink(rssArrSelect, disArrSelect, d0, debug):
    '''
    Input
    ----
        rssArrSelect: selected (training/validation/testing) RSS array
        disArrSelect: selected (training/validation/testing) link distance array
        d0: reference distance
        debug: boolean, whether to show meta info
    Output
    ----
        FadLoss: total fading loss array
        np.mean(FadLoss): mean of the total fading loss
        FadLoss.var(): variance of the total fading loss
        n_p, refLoss: two parameters of the log-distance path loss model
    '''
    # linear regression
    coeffs  = np.polyfit(-10.0*np.log10(disArrSelect/d0), rssArrSelect, 1)
    n_p     = coeffs[0]
    refLoss = coeffs[1] #P0
    
    # estimated average received power
    RSSEst = n_p*(-10.0*np.log10(disArrSelect/d0)) + refLoss
    FadLoss = RSSEst - rssArrSelect 
    
    if debug:
        e_var   = np.mean(np.abs(FadLoss)**2) # mean squared error = Var
        print('[calcPathLossRecLink] MSE and FadLossVar', e_var, FadLoss.var())
    
    return FadLoss, np.mean(FadLoss), FadLoss.var(), n_p, refLoss

# Discretize the area of interest as a grid
def calcGridPixelCoordsNoCHM(LLBound, URBound, delta_p, debug=0):
    '''
    Input
    ----
        LLBound: coordinates of the lower left boundary
        URBound: coordinates of the upper right boundary
        delta_p: pixel resolution
        debug:   boolean, whether to show meta info
    Output
    ----
        pix2d:      num_pixels x 2, coordinates of the discretized field
        distPixels: distance between each pixel pair
        xVals:      X values of the field
        yVals:      Y values of the field
    
    '''
    
    # Set up pixel locations as a grid.
    if debug:
        print('[calcGridPixelCoordsNoCHM] length: {}m, width: {}m'.format(URBound[0]-LLBound[0], 
                                                                        URBound[1]-LLBound[1]))
    
    xVals        = np.arange(LLBound[0], URBound[0], delta_p) #+delta_p/2
    yVals        = np.arange(LLBound[1], URBound[1], delta_p) #+delta_p/2
    cols         = len(xVals)
    rows         = len(yVals) #number of rows of pixels
    pixels       = cols * rows 
    if debug:
        print('[calcGridPixelCoordsNoCHM] field size {}*{}={}'.format(cols, rows, pixels))
    
    # vector: fill the 1st row, then the 2nd row, 3rd, etc.
    xv, yv = np.meshgrid(xVals, yVals,copy=False)
    pix2d = np.c_[xv.reshape(-1), yv.reshape(-1)]
    
    distPixels  = dist.cdist(pix2d, pix2d)
    if debug:
        print('[calcGridPixelCoordsNoCHM] pixelCoords', np.shape(pix2d))
        
    return pix2d, distPixels, xVals, yVals

# Calculate the covariance matrix of the loss field
def calVoxelIndLinkFast2(distPixels, excessPathLen,
                         TXCoords, RXCoords,
                         XVal, YVal, 
                         SelectInd, delta_p, 
                         model, debug=0):
    '''
    Input
    ----
        distPixels:    num_pixels x num_pixels, distance between each pixel pair
        excessPathLen: constant, excess length for the chosen weight model
        TXCoords:      num_links x 2, selected TX coordinates
        RXCoords:      num_links x 2, selected RX coordinates
        XVal:          X values of the field
        YVal:          Y values of the field
        SelectInd:     selected link indices for training/validation
        delta_p:       distance between pixel centers (meters)
        model:         which weight model to select
        debug:         boolean, whether to show meta info
        
    Output
    ----
        voxelInd:      num_links x num_pixels, binary selection matrix
        voxelInd*val:  num_links x num_pixels, binary selection matrix*weight
    '''
    
    ncol, nrow = len(XVal), len(YVal)
    num_links  = len(SelectInd)
    
    xVals = np.tile(XVal, (num_links, 1)) #num_links x num_pixelX
    yVals = np.tile(YVal, (num_links, 1)) #num_links x num_pixelY
     
    TXCoordsel = TXCoords[SelectInd,:]
    RXCoordsel = RXCoords[SelectInd,:]
    
    if model=='ellipse':
        
        TXidx, TXidy  = np.argmin(abs(xVals-TXCoordsel[:,0:1]), axis=1), np.argmin(abs(yVals-TXCoordsel[:,1:2]), axis=1)
        RXidx, RXidy  = np.argmin(abs(xVals-RXCoordsel[:,0:1]), axis=1), np.argmin(abs(yVals-RXCoordsel[:,1:2]), axis=1)
        indTX, indRX  = TXidy*ncol+TXidx, RXidy*ncol+RXidx
        disTXRX = distPixels[indTX,indRX].reshape(-1,1)
        disTXRX[indTX==indRX] = delta_p/2
        
        ePL = distPixels[indTX,:]+distPixels[indRX,:]-disTXRX
        voxelInd = ePL<excessPathLen
     
    return voxelInd, voxelInd*np.power(disTXRX,-0.5)

# Helper function for Bayesian regression
def conjugate_gradient(A, b, x0, tol=1e-5, max_iter=1000):
    """
    Solve the linear system Ax = b using the conjugate gradient method.
    
    Input
    ----
        A : a symmetric positive definite matrix, b is the right-hand side vector, 
        x0: the initial guess for the solution, tol is the tolerance for the solution,
        max_iter is the maximum number of iterations.
    """
    x = x0.copy()
    r = b - np.dot(A, x)
    p = r.copy()
    r_dot_r_prev = np.dot(r.T, r)
    for i in range(max_iter):
        Ap = np.dot(A, p)
        alpha = r_dot_r_prev / np.dot(p, Ap)
        x = x + alpha * p
        r = r - alpha * Ap
        r_dot_r = np.dot(r.T, r)
        if np.sqrt(r_dot_r) < tol:
            break
        beta = r_dot_r / r_dot_r_prev
        p = r + beta * p
        r_dot_r_prev = r_dot_r
    return x

# Bayesian regression for large-scale datasets
def bayesian_linear_regression(X, y, C_inv, regularizer):
    """
    Solve the Bayesian linear regression problem using the conjugate gradient method.
    Input
    ----
        X: the design matrix
        y: the response vector
        C_inv: the inverse of the prior covariance matrix.
    """
    A = X.T @ X + regularizer*C_inv
    b = X.T @ y
    x0 = np.zeros(X.shape[1])
    beta = conjugate_gradient(A, b, x0)
    return beta

# Main loss field estimation function
def CELF(fadLoss, 
         delta_opt, ratioShad_opt, 
         sigmadB2, excessPathLen, 
         regularizer, debug,
         xVals, yVals,
         DistPixels, pixelCoords,
         TXCoords, RXCoords,
         delta_p, model, 
         k_fold=5):
    '''
    Input
    ----
        fadLoss:       (num_links,), fading loss vector
        delta_opt:     constant, space constant
        ratioShad_opt: constant, shadowing variance/ fading loss variance
        sigmadB2:      variance of the fading loss vector
        excessPathLen: constant, excess length for the chosen weight model
        regularizer:   constant, regularization
        debug:         boolean, whether to show meta info
        xVals:         num_pixelX, X values of the field
        yVals:         num_pixelY, Y values of the field
        DistPixels:    num_pixels x num_pixels, distance between each pixel pair
        pixelcoords:   num_pixels x 2, coordinates of the discretized field
        TXCoords:      num_links x 2, TX coordinates
        RXCoords:      num_links x 2, RX coordinates
        delta_p:       distance between pixel centers (meters)
        model:         which weight model to select
        k_fold:        constant, k-fold cross-validation for hyperparameters selection
        
    Output
    ----
        FieldEst:         estimated loss field
        fadLossTrainEst:  estimated fading loss based on the training dataset
        metric_arr:       2 x k_fold, MSE metric
    '''
  
    num_links = len(fadLoss)
    
    if k_fold!=1:
        # shuffle data 
        np.random.seed(20) #for reproducibility
        shuffled_indices = np.arange(num_links)
        np.random.shuffle(shuffled_indices)

        # define k_fold indices
        start_ind = [i for i in np.arange(k_fold)*(num_links//k_fold)]
        end_ind = [i + (num_links//k_fold) for i in start_ind]
        end_ind[-1] = num_links

    metric_arr = np.zeros((2, k_fold+1)) # training and validation RMSE for each fold
    
    for fold in np.arange(k_fold):

        # evaluate train and val indices
        if k_fold!=1:
            val_indices = shuffled_indices[start_ind[fold]:end_ind[fold]]
            train_indices = np.setdiff1d(shuffled_indices, val_indices)
            fadLossVal = fadLoss[val_indices]
        else:
            train_indices = np.arange(num_links)

        
        fadLossTrain = fadLoss[train_indices]
        num_train = len(fadLossTrain)
    
        # Check effective voxels
        indarray, Effvoxels = calVoxelIndLinkFast2(DistPixels, excessPathLen,
                                                   TXCoords, RXCoords,
                                                   xVals, yVals, 
                                                   train_indices, delta_p, 
                                                   model, debug=debug)
        
        if debug:
            ll = 5
            plt.figure()
            plt.plot(pixelCoords[:,0],pixelCoords[:,1],'.')
            plt.plot(pixelCoords[indarray[ll],0],pixelCoords[indarray[ll],1],'*',
                     label='Valid voxels')
            plt.plot(TXCoords[ll,0],TXCoords[ll,1],'s', label='TX')
            plt.plot(RXCoords[ll,0],RXCoords[ll,1],'H', label='RX')
            plt.title('Valid voxels between TX and RX',fontsize=15)
            plt.legend(fontsize=13)
  
        
        # The loss field
        VarShad = ratioShad_opt*sigmadB2
        CovLoss = VarShad/delta_opt*np.exp(-DistPixels/delta_opt) # covariance matrix
        if debug:
            print('[lossfield] CovLoss.shape',CovLoss.shape)

        if num_links>1e6:
            #===============================
            ## conjugate_gradient algorithm
            #===============================
            C_inv = linalg.inv(CovLoss)
            FieldEst = bayesian_linear_regression(Effvoxels, fadLossTrain, C_inv, regularizer)
        elif num_links>len(pixelCoords):
            #======================
            ### MMSE estimator
            #======================
            # Cholesky Factorization for MMSE
            ToBeInverse = Effvoxels.T@Effvoxels + regularizer*linalg.inv(CovLoss)
            L = scipy.linalg.cholesky(ToBeInverse, lower=True)
            FieldEst = scipy.linalg.cho_solve((L, True), Effvoxels.T@fadLossTrain)
        else:
            #========================
            ## Minimum Norm Solution 
            #========================
            operator = CovLoss@Effvoxels.T@linalg.inv(Effvoxels@CovLoss@Effvoxels.T+regularizer*np.eye(num_train))
            FieldEst = operator@fadLossTrain # MxL x Lx1 = Mx1
        
        # Find the shadow loss between TX and RX using effective voxels
        ShadLoss = Effvoxels@FieldEst # L x M

        #----------- Total Fading Loss -----------------#
        fadLossTrainEst = ShadLoss 
        
        # training MSE
        metric_arr[0, fold] = np.mean(np.abs(fadLossTrain-fadLossTrainEst)**2)

        if k_fold!=1:
            # Check effective voxels 
            indarray, valvoxels = calVoxelIndLinkFast2(DistPixels, excessPathLen,
                                                       TXCoords, RXCoords,
                                                       xVals, yVals, pixelCoords,
                                                       val_indices, delta_p, 
                                                       model, debug)
                    
                
            if debug:
                print('[Debug] valvoxels FieldEst .shape',np.shape(valvoxels), np.shape(FieldEst))
            ShadLossval = valvoxels@FieldEst

            fadLossvalEst = ShadLossval 
            
            # validation MSE
            metric_arr[1, fold] = np.mean(np.abs(fadLossVal-fadLossvalEst)**2) 

    
    metric_arr[0, fold+1] = fadLossTrain.var() #training var
    if k_fold!=1:
        metric_arr[1, fold+1] = fadLossVal.var() #val var
    
    return FieldEst, fadLossTrainEst, metric_arr
    
# Fading loss prediction
def predShadLossLinkFast(LossField, excessPathLen,
                         xVals, yVals,
                         distPixels, pixelCoords,
                         TXCoords, RXCoords,
                         delta_p, 
                         model, debug=0):
    '''
    Input
    ----
        LossField:     the loss field estimate
        excessPathLen: constant, excess length for the chosen weight model
        xVals:         num_pixelX, X values of the field
        yVals:         num_pixelY, Y values of the field
        distPixels:    num_pixels x num_pixels, distance between each pixel pair
        pixelcoords:   num_pixels x 2, coordinates of the discretized field
        TXCoords:      num_links x 2, TX coordinates in the test set
        RXCoords:      num_links x 2, RX coordinates in the test set
        delta_p:       distance between pixel centers (meters)
        model:         which weight model to select
        debug:         boolean, whether to show meta info

    Output
    -----
        FadLossTest:   estimated fading loss based on the testing dataset=
    '''
                         
    Indices = np.arange(len(TXCoords))
    _, Effvoxels = calVoxelIndLinkFast2(distPixels, excessPathLen,
                                        TXCoords, RXCoords,
                                        xVals, yVals, 
                                        Indices, delta_p, 
                                        model, debug=debug)
    if debug:
        print('[predShadLossLinkFast] Effvoxels.shape',Effvoxels.shape)
    
    #----------- Shadow Loss -----------------#
    # Find the shadow loss between TX and RX using effective voxels
    ShadLoss = Effvoxels@LossField
    
    #----------- Total Fading Loss -----------------#
    FadLossTest = ShadLoss 

    return FadLossTest

# Campus terrain profile loading
def CampusMapLoading(terrainfile, BLCoordCamp, TRCoordCamp):
    # terrain profile loading
    dsm_object = rio.open(terrainfile)
    dsm_map = dsm_object.read(1)     # a np.array containing elevation values
    dsm_map -= dsm_map.min()
    dsm_resolution = dsm_object.res     # a tuple containing x,y resolution (0.5 meters) 
    print('min and max', np.max(dsm_map),np.min(dsm_map), np.max(dsm_map)-np.min(dsm_map))

    dsm_transform = dsm_object.transform     # an Affine transform for conversion to UTM-12 coordinates
    utm_transform = np.array(dsm_transform).reshape((3,3))[:2]
    utm_top_left = utm_transform @ np.array([0,0,1])

    # use utm 12 T for precision check
    TLCoord = utm_top_left

    # campus map boudary
    BLCampIdx = (int(round((BLCoordCamp[0]-TLCoord[0])/dsm_resolution[0])), \
                int(round((TLCoord[1]-BLCoordCamp[1])/dsm_resolution[1])))
    TRCampIdx = (int(round((TRCoordCamp[0]-TLCoord[0])/dsm_resolution[0])), \
                int(round((TLCoord[1]-TRCoordCamp[1])/dsm_resolution[1])))
    print('BLCampIdx, TRCampIdx',BLCampIdx, TRCampIdx)

    # campus map
    campmap = dsm_map.T[BLCampIdx[0]:TRCampIdx[0], TRCampIdx[1]:BLCampIdx[1]]
    MapCamp = np.flip(campmap, axis=1).T
    print('MapCamp shape', np.shape(MapCamp))

    return MapCamp, dsm_resolution

# Plot the loss field and the campus map as a reference
def LossFieldPLot(MapCamp, dsm_resolution, LossField, yVals, xVals, delta_p, label):
    # preparation
    nrows, ncols = len(yVals), len(xVals)
    print('label nrows ncols',label, nrows, ncols, nrows*ncols)

    ratio_field2chm = delta_p//np.array(dsm_resolution)
    contour_x, contour_y = np.arange(0,ncols)*ratio_field2chm[0], np.arange(0,nrows)*ratio_field2chm[1]
    mesh_x, mesh_y = np.meshgrid(contour_x, contour_y)

    nrows_chm, ncols_chm = np.shape(MapCamp)
    print('nrows_chm, ncols_chm',nrows_chm, ncols_chm)

    # contourf plot
    fig, ax = plt.subplots(1,2, figsize=(9.6,4.8), sharex=True, sharey=True)
    CS = ax[1].contourf(mesh_x, mesh_y, LossField.reshape(nrows, ncols),cmap='viridis')
    ax[1].grid()
    ax[1].set_xlim([0,ncols])
    ax[1].set_ylim([0,nrows]) # upper-flip once, ylim-flip twice
    xticks = np.arange(0,ncols,12)
    yticks = np.arange(0,nrows+10,12)
    ax[1].set_xticks(xticks)
    ax[1].set_yticks(yticks)
    xtick_labels = [str(i*delta_p) for i in xticks]
    ytick_labels = [str(i*delta_p) for i in yticks]
    ax[1].set_xticklabels(xtick_labels,fontsize=13)
    ax[1].set_yticklabels(ytick_labels,fontsize=13)


    CS1 = ax[0].imshow(MapCamp, cmap="Greys",
            alpha=1, interpolation = None,
            origin='lower', aspect='1.1')

    ax[0].set_ylabel('UTM$_N$ (m)', fontsize=15)
    ax[0].grid()

    ax[0].set_xlim([0, ncols_chm])
    ax[0].set_ylim([0, nrows_chm])
    xticks_chm = np.arange(0,ncols_chm,600)
    yticks_chm = np.arange(0,nrows_chm+200,600)
    xtick_labels_chm = [str(int(i*dsm_resolution[0])) for i in xticks_chm]
    ytick_labels_chm = [str(int(i*dsm_resolution[1])) for i in yticks_chm]
    ax[0].set_xticks(xticks_chm) #,fontsize=12,rotation=0)
    ax[0].set_yticks(yticks_chm)
    ax[0].set_xticklabels(xtick_labels_chm,fontsize=12,rotation=0)
    ax[0].set_yticklabels(ytick_labels_chm,fontsize=12,rotation=0)

    fig.text(0.5, 0.015, 'UTM$_E$ (m)', va='center', \
            ha='center', rotation='horizontal', fontsize=15)
    plt.tight_layout()
    plt.show()


######################### Example Test ####################################
def SubsetProcessing(data_ins, label, TRCoordCamp1, BLCoordCamp1, terrain_file, d0=1):
    # choose one subset 
    print('dataset rss & dis shape', label, np.shape(data_ins.rss[label]), np.shape(data_ins.dis[label]),'\n')
    #============================================
    # Split links: Train - Test
    #============================================
    # the seed for 'cbrssdr' is different to ensure that
    # the variances of the training and test datasets are close
    if label=='cbrssdr':
        seed = 462
    else:
        seed = 472
    np.random.seed(seed) #for reproducibility
    num_links = len(data_ins.rss[label])
    
    splitRatio = [70,0,30]
    shuffledIndices = np.arange(num_links)
    
    np.random.shuffle(shuffledIndices)
    splitInd = np.cumsum(splitRatio/np.sum(splitRatio)*num_links).astype('uint32')
    print('splitInd',splitInd,'\n')

    TrainIndices = shuffledIndices[:splitInd[0]]
    DevIndices = shuffledIndices[splitInd[0]:splitInd[1]]
    TestIndices = shuffledIndices[splitInd[1]:]
    assert (TrainIndices.shape[0]+DevIndices.shape[0]+TestIndices.shape[0]) == num_links
    
    # Train-Test separation
    RSSTrain, DisTrain, _, _, RSSTest, DisTest = SepTrainTestLink(data_ins.rss[label], 
                                                                            data_ins.dis[label], 
                                                                            TrainIndices, 
                                                                            DevIndices, 
                                                                            TestIndices)
    print('[Train] DisTrain min and max', DisTrain.min(), DisTrain.max(),'\n')
    
    #============================================
    # Fading loss calculation
    #============================================
    #1 fading loss for loss field learning
    debug = 0 
    FadLossTrain, FadmeanTrain, SigmadB2Train, n_p, refLossLink = calcPathLossRecLink(RSSTrain,
                                                                                    DisTrain,
                                                                                    d0, 
                                                                                    debug)
    print('[Train] refLoss, n_p, errstd',refLossLink, n_p)
    print('[Train] fadLoss shape, fadmean and sigmadB',np.shape(FadLossTrain), 
        np.round(FadmeanTrain,2), (SigmadB2Train),'\n')

    #2 fading loss for accuracy evaluation
    FadLossTest, FadmeanTest, SigmadB2Test, n_p1, refLoss1 = calcPathLossRecLink(RSSTest,
                                                                                DisTest,
                                                                                d0,
                                                                                debug)
    print('[Test] refLoss, n_p, errstd',refLoss1, n_p1)
    print('[Test] fadLoss shape, fadmean and sigmadB2',np.shape(FadLossTest), np.round(FadmeanTest,2), SigmadB2Test,'\n')


    #==== Step 3: loss field learning ====#
    debug = 0
    model = 'ellipse'
    # hyperparameters for the algorithm after validation
    if model=='ellipse':
        ######## ORIGINAL Ellipse model ###########
        ## rooftop
        if label=='cbrssdr':
            delta_p, excessPathLen = 25, 105 #best
            delta_opt, ratioShad_opt = 35, 0.58
            Regularizer = 0.3
        ## nuc
        if label=='nuc':
            delta_p, excessPathLen = 25, 150#125 # utm
            delta_opt, ratioShad_opt = 15, 0.4
            Regularizer = 0.6 #1.5
        ## bus
        if label=='bus':
            delta_p, excessPathLen = 25, 145 # utm 
            delta_opt, ratioShad_opt = 13, 0.37
            Regularizer = 0.45 #1.5 #
        ## DD
        if label=='dd':
            delta_p, excessPathLen = 25, 185 # utm
            delta_opt, ratioShad_opt = 13, 0.46
            Regularizer = 0.3

    BLCoordConvert = (0,0)
    TRCoordConvert = np.array(TRCoordCamp1) - np.array(BLCoordCamp1)

    pixelCoords, DistPixels, xVals, yVals = calcGridPixelCoordsNoCHM(BLCoordConvert, TRCoordConvert, 
                                                                    delta_p, debug=debug)
    print('[Train] DistPixels', np.shape(DistPixels))

    RxCoordsTrain1 = data_ins.rxcoords[label][TrainIndices,:] - np.array(BLCoordCamp1)
    TxCoordsTrain1 = data_ins.txcoords[label][TrainIndices,:] - np.array(BLCoordCamp1)
    print('[Train] # of samples', len(TxCoordsTrain1))

    t0 = time.time()
    LossField, FadLossTrainEst, metric_arr = CELF(FadLossTrain,
                                                delta_opt, ratioShad_opt,
                                                SigmadB2Train, excessPathLen,
                                                Regularizer, debug,
                                                xVals, yVals,
                                                DistPixels, pixelCoords,
                                                TxCoordsTrain1, RxCoordsTrain1,
                                                delta_p, 
                                                model, k_fold=1)

    print('[Train] Training time', time.time()-t0)
    print('[Helper] delta_p excessPath Regularizer metric:' , delta_p, excessPathLen, Regularizer,metric_arr,'\n\n')
    print('[Helper] mean LossFieldMNE', np.mean(LossField))

    errTrain = np.sqrt(np.mean(np.abs(FadLossTrain-FadLossTrainEst)**2))
    print('[Train] FadEstErr in dB and dB2', errTrain, errTrain**2)
    print('[Train] std/var of fadloss in dB',FadLossTrain.std(), FadLossTrain.var())
    print('[Train] percent decreased', (FadLossTrain.std()-errTrain)/FadLossTrain.std(), 
        (FadLossTrain.var()-errTrain**2)/FadLossTrain.var())

    #==== Step 4: fading loss prediction ====#
    TxCoordsTest1 = data_ins.txcoords[label][TestIndices,:] - np.array(BLCoordCamp1)
    RxCoordsTest1 = data_ins.rxcoords[label][TestIndices,:] - np.array(BLCoordCamp1)
    print('[Test] txcoords and fadloss shape',np.shape(TxCoordsTest1), np.shape(FadLossTest))

    # fading loss prediction
    t0 = time.time()
    FadLossTestEst = predShadLossLinkFast(LossField, 
                                        excessPathLen,
                                        xVals, yVals,
                                        DistPixels, pixelCoords,
                                        TxCoordsTest1, RxCoordsTest1,
                                        delta_p, 
                                        model, debug=1)
    print("[Test time]", time.time()-t0)

    #----------- Fading Loss Estimation Error -----------------#
    FadTestErr = np.sqrt(np.mean(np.abs(FadLossTest-FadLossTestEst)**2))


    print('\n[Test] label model', label, model)
    print('[Train] FadEstErr in dB and dB2', errTrain, errTrain**2)
    print('[Train] std/var of fadloss in dB',FadLossTrain.std(), FadLossTrain.var())
    print('[Train] percent decreased', (FadLossTrain.std()-errTrain)/FadLossTrain.std(), 
        round((FadLossTrain.var()-errTrain**2)/FadLossTrain.var(),4))

    print('\n[Test] FadTestErr in dB',FadTestErr, FadTestErr**2)
    print('[Test] std of FadLossTest',FadLossTest.std(), FadLossTest.var())
    print('[Test] percent decreased', (FadLossTest.std()-FadTestErr)/FadLossTest.std(), 
        round((FadLossTest.var()-FadTestErr**2)/FadLossTest.var(),4) )


    plt.figure(figsize=(6,4))
    plt.plot(FadLossTest, FadLossTestEst, '.')
    plt.ylim([-30,30])
    plt.xlim([-30,30])
    # plt.legend(fontsize=15)
    plt.ylabel('Predicted Fading Loss (dB)',fontsize=18)
    plt.xlabel('True Fading Loss (dB)',fontsize=18)
    plt.grid()
    plt.tight_layout()

    # check the loss field estimate
    CampMap, MapRes = CampusMapLoading(terrain_file, BLCoordCamp1, TRCoordCamp1)
    LossFieldPLot(CampMap, MapRes, LossField, yVals, xVals, delta_p, label)

def main():
    # data file
    data_file = 'data/campus.json' # TODO: specify the file path
    # terrain profile
    terrain_file = 'data/CHM.tif' # TODO: specify the file path

    # Utah campus boudary
    BLLatLonCamp = (40.75413, -111.85390)
    TRLatLonCamp = (40.77500, -111.82632)
    BLCoordCamp = utm.from_latlon(BLLatLonCamp[0], BLLatLonCamp[1])[:2]
    TRCoordCamp = utm.from_latlon(TRLatLonCamp[0], TRLatLonCamp[1])[:2]
    BLCoordCamp1 = (BLCoordCamp[0], BLCoordCamp[1]+200)
    TRCoordCamp1 = (BLCoordCamp1[0]+2200, BLCoordCamp1[1]+2100)

    # four receiver subsets given different RX types
    labels = ['cbrssdr', 'nuc', 'bus', 'dd']

    # noise ratio
    NoiseRatio = [0.2, 0.2, 0.2, 0.2]

    # Path loss settings
    Freq = 462.7e6
    d0 = 1

    #==== Step 1: data loading ====#
    data_ins = Data(data_file, BLCoordCamp1, TRCoordCamp1, labels, d0=d0, noiseratio=NoiseRatio)
    data_ins.calcData_wLabels()

    for label in labels:
        print('# of samples', len(data_ins.rss[label]))


    #==== Step 2: loss field learning ====#
    # TODO: specify which subset to look at
    Subset = labels[3] # options 0-rooftop, 1-fixed endpoints, 2-bus nodes, 3-dense deployment
    SubsetProcessing(data_ins, Subset, TRCoordCamp1, BLCoordCamp1, terrain_file, d0=d0)
    
if __name__ == "__main__":
    main()